package group21.lvtn.wowauthentication;

import android.support.v7.app.ActionBarActivity;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import group21.lvtn.wowauthentication.algorithm.Eigenface;
import group21.lvtn.wowauthentication.algorithm.Fisherface;
import group21.lvtn.wowauthentication.utils.InputHandle;

import static org.bytedeco.javacpp.opencv_imgcodecs.CV_LOAD_IMAGE_GRAYSCALE;
import static org.bytedeco.javacpp.opencv_imgcodecs.imread;

public class MainActivity extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//setContentView(R.layout.activity_main);
		/*if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id., new PlaceholderFragment()).commit();
		}*/
		runEigenface();
		//runFisherface();

	}
	private void runEigenface(){
		InputHandle input = new InputHandle();

		Eigenface eigen = new Eigenface(input.getSrc(), input.getLables(), 10, 1000.f);

		int result = eigen.predict(imread("/storage/emulated/0/Pictures/CameraAPIDemo/1-3.jpg",
				CV_LOAD_IMAGE_GRAYSCALE));

		Toast.makeText(getBaseContext(),"RUn: " + result, Toast.LENGTH_LONG).show();
	}

	private void runFisherface() {
		InputHandle input = new InputHandle();

		Fisherface fish = new Fisherface(input.getSrc(), input.getLables(), input.getNumber_of_classes(), 10, 1000.f);

		int result = fish.predict(imread("/storage/emulated/0/Pictures/CameraAPIDemo/3-3.jpg",
				CV_LOAD_IMAGE_GRAYSCALE));

		Toast.makeText(getBaseContext(), "Run: " + result, Toast.LENGTH_LONG).show();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);
			return rootView;
		}
	}
}
