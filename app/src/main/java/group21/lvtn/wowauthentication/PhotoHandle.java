package group21.lvtn.wowauthentication;

import android.app.Application;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.face.Face;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import cn.pedant.SweetAlert.SweetAlertDialog;


public class PhotoHandle  implements  CameraSource.PictureCallback {

    private final Context context;
    private final Face myFace;
    private final int CAMERA_WIDTH;
    private final int CAMERA_HEIGHT;
    private  int indexphoto;
    private int type;
    private boolean isSuccess = false;
    private SweetAlertDialog pDialog;

    //Constructors
    public PhotoHandle(Context context) {
        this.context = context;
        this.myFace = null;
        this.CAMERA_HEIGHT = 0;
        this.CAMERA_WIDTH = 0;
    }

    public PhotoHandle(int type, int index, SweetAlertDialog dialog, Context context, Face myFace, int width, int height) {
        this.context = context;
        this.myFace = myFace;
        this.CAMERA_WIDTH = width;
        this.CAMERA_HEIGHT = height;
        this.pDialog = dialog;
        this.indexphoto = index;
        this.type = type;
    }

    public boolean isHandleSuccess(){
        return isSuccess;
    }
    //Main function
    public void onPictureTaken(byte[] data) {
        getImage(data);
    }

    //Call CropImage - AsyncTask
    private void getImage(byte[] data) {
        if(type == 0)
        {
            CropImage crop = new CropImage();
            crop.execute(data);
        }
        else {
            Bitmap photo = BitmapFactory.decodeByteArray(data, 0, data.length);

            int width = photo.getWidth();
            int height = photo.getHeight();
            if(myFace == null) {
                isSuccess = true;
                return;
            }
            float[] myFaceSize = {myFace.getPosition().x, myFace.getPosition().y, myFace.getWidth(), myFace.getHeight()};
            int [] size = process(myFaceSize, width, height);

            Bitmap bitmap = cropImage(photo, size[0], size[1], size[2], size[3]);

            if (bitmap != null) {
                saveImage(bitmap);
            }


        }

    }

    //Get folder output
    public static File getDir() {
        File sdDir = Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        return new File(sdDir, "CameraAPIDemo");
    }

    private int[] process(float[] arr, int width, int height) {
        float scale = height / (float)CAMERA_WIDTH;
        int[] size = new int[4];
        for (int i=0; i<4; i++) {
            size[i] = (int) (arr[i] *scale);
        }
        return size;
    }

    public Bitmap bitmapResize(Bitmap bitmap,int newWidth,int newHeight) {
        Bitmap scaledBitmap = Bitmap.createBitmap(newWidth, newHeight, Bitmap.Config.ARGB_8888);

        float ratioX = newWidth / (float) bitmap.getWidth();
        float ratioY = newHeight / (float) bitmap.getHeight();
        float middleX = newWidth / 2.0f;
        float middleY = newHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bitmap, middleX - bitmap.getWidth() / 2, middleY - bitmap.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

        return scaledBitmap;

    }

    private void saveImage(Bitmap photo){
        File pictureFileDir = getDir();

        if (!pictureFileDir.exists() && !pictureFileDir.mkdirs()) {
            // Log.d(SyncStateContract.Constants.DEBUG_TAG, "Can't create directory to save image.");
            Toast.makeText(context, "Can't create directory to save image.",
                    Toast.LENGTH_LONG).show();
            return;
        }

        // SimpleDateFormat dateFormat = new SimpleDateFormat("yyyymmddhhmmss");
        // String date = dateFormat.format(new Date());
        String photoFile = "1-" + String.valueOf(indexphoto)+ ".jpg";

        String filename = pictureFileDir.getPath() + File.separator + photoFile;

        File pictureFile = new File(filename);

        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);

            photo.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.flush();
            fos.close();
            Log.e("Files", "Save photo authentication");
            isSuccess = true;
            MediaScannerConnection.scanFile(context, new String[]{pictureFile.toString()}, null,
                    new MediaScannerConnection.OnScanCompletedListener() {
                        public void onScanCompleted(String path, Uri uri) {
                            Log.i("ExternalStorage", "Scanned " + path + ":");
                            Log.i("ExternalStorage", "-> uri=" + uri);
                        }
                    });
        } catch (Exception error) {
            Toast.makeText(context, "Image could not be saved.",
                    Toast.LENGTH_LONG).show();
        }
    }

    private Bitmap cropImage (Bitmap image, int x, int y, int width, int height) {

        int offset = Math.abs(width - height)/2;

        //textView.append("Original: " + x + " " + y + " " + width + " " + height);
        //Log.e("Original: ", x + " " + y + " " + width + " " + height);

        if (width > height) {
            y -= offset;
            height = width;
        }
        else {
            width = height;
            x -= offset;
        }
        //textView.append("New: " + x + " " + y + " " + width + " " + height);
        //Log.e("New: ", x + " " + y + " " + width + " " + height);

        Bitmap croppedBitmap;
        try {
            croppedBitmap = Bitmap.createBitmap(image, x, y, width, height);
        }
        catch (Exception error){
            return null;
        }

        Bitmap scale = bitmapResize(croppedBitmap, 50, 50);

        //textView.append("Finale: " + scale.getWidth() + " " + scale.getHeight());
        //Log.e("Finale: ", scale.getWidth() + " " + scale.getHeight());

        return scale;
    }

    //Crop Image in background, without freezing the UI
    private class CropImage extends AsyncTask<byte[], Boolean, Bitmap> {

        @Override
        protected Bitmap doInBackground(byte[]... data) {
            Bitmap photo = BitmapFactory.decodeByteArray(data[0], 0, data[0].length);

            int width = photo.getWidth();
            int height = photo.getHeight();
            if(myFace == null) return null;
            float[] myFaceSize = {myFace.getPosition().x, myFace.getPosition().y, myFace.getWidth(), myFace.getHeight()};

            //Log.e("Original: ", myFace.getPosition().x + " " + myFace.getPosition().y+ " " + myFace.getWidth() + " " + myFace.getHeight());

            int [] size = process(myFaceSize, width, height);

            Bitmap cropbMap = cropImage(photo, size[0], size[1], size[2], size[3]);

            return cropbMap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);

            if (bitmap != null) {
                saveImage(bitmap);
                if(pDialog != null) pDialog.cancel();
            }
            else {

               // Toast.makeText(context, "Please place your face in the center of the camera", Toast.LENGTH_SHORT).show();
                if(pDialog != null)
                {
                    pDialog.cancel();
                    new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Oops...")
                            .setContentText("Please place your face in the center of the camera!")
                            .show();
                }

            }
        }


        private Bitmap getBitmap(byte[] data) {
            Bitmap photo = BitmapFactory.decodeByteArray(data, 0, data.length);

            int width = photo.getWidth();
            int height = photo.getHeight();
            float[] myFaceSize = {myFace.getPosition().x, myFace.getPosition().y, myFace.getWidth(), myFace.getHeight()};

            //Log.e("Original: ", myFace.getPosition().x + " " + myFace.getPosition().y+ " " + myFace.getWidth() + " " + myFace.getHeight());

            int [] size = process(myFaceSize, width, height);

            Bitmap cropbMap = cropImage(photo, size[0], size[1], size[2], size[3]);

            return cropbMap;
        }


    }
}
