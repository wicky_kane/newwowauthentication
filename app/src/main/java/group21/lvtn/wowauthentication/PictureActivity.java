package group21.lvtn.wowauthentication;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

public class PictureActivity extends AppCompatActivity {

    ImageView imageView;
    TextView textView;
    String[] info;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_picture);

        imageView = (ImageView) findViewById(R.id.picture_captured);
        textView = (TextView) findViewById(R.id.picture_size);

        Intent intent = this.getIntent();

        if (intent != null & intent.hasExtra("IMG_DIR") ) {
            info = intent.getStringArrayExtra("IMG_DIR");

            //Height 1600, width 1200
            Bitmap bMap = BitmapFactory.decodeFile(info[4]);

            int x= cal(info[0]).intValue();
            int y= cal(info[1]).intValue();
            int width= cal(info[2]).intValue();
            int height= cal(info[3]).intValue();

            Bitmap cropbMap = cropImage(bMap, x, y, width, height);

            imageView.setImageBitmap(cropbMap);
        }
    }

    private Bitmap cropImage (Bitmap image, int x, int y, int width, int height) {
        Matrix matrix = new Matrix();
        //matrix.postScale(0.5f, 0.5f)
        int offset = Math.abs(width - height)/2;

        textView.setText("Original: " + x + " " + y + " " + width + " " + height);

        if (width > height) {
            y -= offset;
            height = width;
        }
        else {
            width = height;
            x -= offset;
        }

        textView.append("New: " + x + " " + y + " " + width + " " + height);

        Bitmap croppedBitmap = Bitmap.createBitmap(image, x, y, width, height, matrix, true);

        return croppedBitmap;
    }

    //2.5 la ti le camera voi hinh.
    //Camera 640x480
    //Hinh 1600x1200
    private Double cal (String a) {
        return (new Double(a)) * 2.5;
    }

}
