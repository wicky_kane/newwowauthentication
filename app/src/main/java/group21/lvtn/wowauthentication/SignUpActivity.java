package group21.lvtn.wowauthentication;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Switch;

import cn.pedant.SweetAlert.*;

@SuppressLint("NewApi")
public class SignUpActivity extends AppCompatActivity {
 
    
    private Button btnSignup;
    private Button btnAuthen;
    private Switch mySwitch;
    private PrefManager prefManager;
    private SweetAlertDialog pDialog;
    final private String SIGN_UP_INFO = "You must sign up first!";
    static final int SIGNUP_REQUEST = 1;
   
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
 
        // Checking for first time launch - before calling setContentView()
        prefManager = new PrefManager(this);

        // Making notification bar transparent
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
 
        setContentView(R.layout.activity_signup);

        btnAuthen = (Button) findViewById(R.id.btn_authentication);
        btnSignup = (Button) findViewById(R.id.btn_singup);
        mySwitch = (Switch) findViewById(R.id.mySwitch);
  
        // making notification bar transparent
        changeStatusBarColor();

        btnSignup.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //prefManager.setSignedUp(false);
                if (prefManager.isSignedUp()) {
                    showDialogWithConfirm();
                }
                else
                    launchFaceTrackerActivity();

            }
        });

        btnAuthen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchAuthenticationActivity();
            }
        });

        mySwitch.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
				// TODO Auto-generated method stub
				 if(arg1){
					 if(!prefManager.isSignedUp()){	
						 	showDialog(SIGN_UP_INFO);
						 	mySwitch.setChecked(false);
				    		mySwitch.setText(mySwitch.getText());
				    		return;
					 }
					 		//Toast.makeText(getApplicationContext(), "Turn On", Toast.LENGTH_SHORT).show();
					    
				    }else{
				    		//Toast.makeText(getApplicationContext(), "Turn Off", Toast.LENGTH_SHORT).show();
				 }
			}
		});
       
    }
 
    
    private void showDialog(String msg){
    	SweetAlertDialog sd = new SweetAlertDialog(SignUpActivity.this);
        sd.setCancelable(true);
        sd.setCanceledOnTouchOutside(true);
        sd.setTitleText(msg);
        sd.show();
    }
    private void showDialogWithConfirm(){
        pDialog = new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE);
        pDialog.setTitleText("Do you want to re-signup?")
        .setContentText("You have already signed up!")
        .setCancelText("No")
        .setConfirmText("Yes")
        .showCancelButton(true)
        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
            public void onClick(SweetAlertDialog sDialog) {
                sDialog.cancel();
            }
        }).setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                launchAuthenReSignUp();
            }
        })
        .show();
    }
    private void launchFaceTrackerActivity() {
        startActivity(new Intent(this, FaceTrackerActivity.class));
        //finish();
    }
    private void launchAuthenReSignUp() {
        Intent intent = new Intent(this, AuthenticationActivity.class);
        startActivityForResult(intent, SIGNUP_REQUEST);
    }
    private void launchAuthenticationActivity() {
        startActivity(new Intent(this, AuthenticationActivity.class));
    }
    /**
     * Making notification bar transparent
     */
    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode) {
            case (SIGNUP_REQUEST) : {
                if (resultCode == Activity.RESULT_OK) {
                    if(pDialog != null) pDialog.cancel();
                    launchFaceTrackerActivity();
                }
                break;
            }
          default:
                break;
            }
        }

}
