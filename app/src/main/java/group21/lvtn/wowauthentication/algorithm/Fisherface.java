package group21.lvtn.wowauthentication.algorithm;

import android.util.Log;

import org.bytedeco.javacpp.opencv_core;

import java.nio.IntBuffer;
import java.util.Vector;

import static org.bytedeco.javacpp.opencv_core.GEMM_1_T;
import static org.bytedeco.javacpp.opencv_core.LDA;
import static org.bytedeco.javacpp.opencv_core.PCA;
import static org.bytedeco.javacpp.opencv_core.gemm;

import org.bytedeco.javacpp.opencv_core.Mat;
import org.bytedeco.javacpp.opencv_core.MatVector;

/**
 * Created by Lotso Chik on 9/2/2016.
 */
public class Fisherface {

    private static int _num_components;
    private static double _threshold;
    private static MatVector _projection;
    private static Mat _labels;
    private static Mat _eigenvectors;
    private static Mat _mean;

    private static int number_of_classes;

    public Fisherface(Vector<Mat> src, Mat labels, int classes, int num_compenents, double threshold){
        this._num_components = num_compenents;
        this._threshold = threshold;
        this.number_of_classes = classes;

        compute(src, labels);
    };

    public static Mat asRowMatrix(Vector<Mat> src, int rtype){
        double alpha = 1.0;
        double beta = 0.0;

        int n = src.size();
        if(n == 0)
            return new Mat();
        int d = (int) src.get(0).total();

        Mat data = new Mat(n, d, rtype);
        for(int i = 0;i < n; i++){
            if((int)src.get(i).total() != d){
                throw new RuntimeException("Sai chieu dai d");
            }
            Mat xi = data.row(i);
            if(src.get(i).isContinuous()){
                src.get(i).reshape(1,1).convertTo(xi, rtype, alpha, beta);
            } else {
                src.get(i).clone().reshape(1,1).convertTo(xi, rtype, alpha, beta);
            }
        }
        return data;
    }

    private void compute(Vector<Mat> src, Mat labels){
        if(src.size() == 0 ){
            throw new RuntimeException("Empty training data was given.");
        }

        //src la vector chua 30 Mat 200x180
        Log.e("SRC", src.size() + " " + src.get(0).rows() + " " + src.get(0).cols()); //30 200 180

        //Chuyen thanh Mat 30 row, moi row la 1 hinh: 200x180=36000
        Mat data = asRowMatrix(src, opencv_core.CV_64FC1);
        Log.e("DATA", data.rows() + " " + data.cols()); //30 + 36000

        Log.e("LABELS", labels.rows() +" " + labels.cols()); //30 + 1

        Log.e("CLASS", number_of_classes + " " ); //3

        int n = data.rows();
        int d = data.cols();
        if(n != labels.rows()){
            throw new RuntimeException("The number of samples (src) must equal the number of labels (labels).");
        }

        if(_num_components <= 0 || _num_components > (number_of_classes-1) ){
            _num_components = number_of_classes-1;
        }

        //FISHER
        //PCA pca(data, Mat(), CV_PCA_DATA_AS_ROW, (N-C));
        PCA pca = new PCA(data, new Mat(), PCA.DATA_AS_ROW, n - number_of_classes);

        Log.e("DATA sau PCA", data.rows() + " " + data.cols()); //30 + 36000

        Mat _project_data = pca.project(data);

        Log.e("DATA sau PROJECT", _project_data.rows() + " " + _project_data.cols()); //30 + 27

        MatVector data_matvector = mat_to_matvector(_project_data);

        Log.e("DATA sau MATVECTOR", data_matvector.size() + " " + data_matvector.get(0).rows() + " "
            + data_matvector.get(0).cols()); //30 + 1 + 27

        //subspace::LDA lda(pca.project(data), labels, _num_components);
        LDA lda = new LDA(data_matvector, labels, _num_components);

        _mean = pca.mean().reshape(1, 1);
        _labels = labels;

        Log.e("pca.eigenvectors", pca.eigenvectors().rows() + " " + pca.eigenvectors().cols()); //27 + 36000
        Log.e("lda.eigenvectors", lda.eigenvectors().rows() + " " + lda.eigenvectors().cols()); //27 + 2

        _eigenvectors = new Mat(pca.eigenvectors().cols(), lda.eigenvectors().rows(), pca.eigenvectors().type());

        gemm(pca.eigenvectors(), lda.eigenvectors(), 1.0, new Mat(), 0.0, _eigenvectors, GEMM_1_T);

        Log.e("_eigenvectors", _eigenvectors.rows() + " " + _eigenvectors.cols()); //36000 + 2
        Log.e("_mean", _mean.rows() + " " + _mean.cols()); //1 + 36000

        Log.e("data", data.row(0).rows() + " " + data.row(0).cols()); //1 + 36000

        _projection = new MatVector(data.rows());

        for(int sampleIdx = 0; sampleIdx < data.rows(); sampleIdx++) {
            Mat p = lda.subspaceProject(_eigenvectors, _mean, data.row(sampleIdx).clone());
            _projection.put(sampleIdx, p);
        }

        Log.e("_projection", _projection.size() + " " + _projection.get(0).cols()
            +" " + _projection.get(0).rows()); //30 + 2 + 1
    }

    public int predict(Mat src) {
        int minClass;
        double minDist;

        if(this._projection.isNull()) {
            // throw error if no data (or simply return -1?)
            throw new RuntimeException("Eigenfaces model is not computed yet. Did you call cv::Eigenfaces::train?");

        } else if(_eigenvectors.rows() != src.total()) {
            // check data alignment just for clearer exception messages
            throw new RuntimeException("Wrong input image size. Reason: Training and Test images must be of equal size!");
        }

        //Project into LDA subspace
        LDA lda = new LDA();
        Mat q = lda.subspaceProject(_eigenvectors, _mean, src.reshape(1,1));

        // find 1-nearest neighbor
        minDist = Float.MAX_VALUE;
        minClass = -1;

        IntBuffer labelsBuf = _labels.createBuffer();

        for(int sampleIdx = 0; sampleIdx < _projection.size(); sampleIdx++) {
            double dist = opencv_core.norm(_projection.get(sampleIdx), q, opencv_core.NORM_L2, new Mat());
            //Log.e("Files", "Distance  size " + String.valueOf(dist));
            if((dist < minDist) && (dist < _threshold)) {
                minDist = dist;
                minClass = labelsBuf.get(sampleIdx);
                Log.e("PREDICT LABEL", minClass + " " );
            }
        }

        return minClass;
    }

    private MatVector mat_to_matvector(Mat src) {
        MatVector result = new MatVector(src.rows());

        for (int i = 0 ; i<src.rows(); i++) {
            Mat temp = src.row(i);

            result.put(i, temp);
        }
        return result;
    }

}
