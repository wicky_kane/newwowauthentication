package group21.lvtn.wowauthentication.utils;


import java.io.File;
import java.io.FilenameFilter;
import java.nio.IntBuffer;
import java.util.HashSet;
import java.util.Vector;
import group21.lvtn.wowauthentication.PhotoHandle;

import org.bytedeco.javacpp.opencv_core.Mat;

import static org.bytedeco.javacpp.opencv_imgcodecs.CV_LOAD_IMAGE_GRAYSCALE;
import static org.bytedeco.javacpp.opencv_imgcodecs.imread;
import static org.bytedeco.javacpp.opencv_core.CV_32SC1;

public class InputHandle {
    private Vector<Mat> src;
    private Mat labels;
    private int number_of_classes;

    public InputHandle() {
        
        File root = PhotoHandle.getDir();

        FilenameFilter imgFilter = new FilenameFilter() {
            public boolean accept(File dir, String name) {
                name = name.toLowerCase();
                return name.endsWith(".jpg") || name.endsWith(".pgm") || name.endsWith(".png");
            }
        };

        File[] imageFiles = root.listFiles(imgFilter);
        src = new Vector<>(imageFiles.length);
        labels = new Mat(imageFiles.length, 1, CV_32SC1);
        IntBuffer labelsBuf = labels.createBuffer();

        int counter = 0;
        number_of_classes = 0;
        HashSet<Integer> set_of_labels = new HashSet<>();

        for (File image : imageFiles) {
            Mat img = imread(image.getAbsolutePath(), CV_LOAD_IMAGE_GRAYSCALE);

            int label = Integer.parseInt(image.getName().split("\\-")[0]);

            set_of_labels.add(label);

            src.add(img);
            labelsBuf.put(counter, label);

            counter++;
        }

        number_of_classes = set_of_labels.size();
    }

    public int getNumber_of_classes() {
        return number_of_classes;
    }

    public Mat getLables() {
        return labels;
    }

    public Vector<Mat> getSrc() {
        return src;
    }

}
