package group21.lvtn.wowauthentication.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.bytedeco.javacpp.opencv_core.FileStorage;
import org.bytedeco.javacpp.opencv_core;
import org.bytedeco.javacpp.opencv_core.Mat;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Vector;

import cn.pedant.SweetAlert.SweetAlertDialog;
import group21.lvtn.wowauthentication.PhotoHandle;
import group21.lvtn.wowauthentication.algorithm.Eigenface;

/**
 * Created by wicky on 9/14/2016.
 */
public class StoreModel {
    public static void saveEigenface(String file_name,Eigenface eigen)
    {
        FileStorage fs = new FileStorage(file_name,FileStorage.WRITE);
        opencv_core.write(fs,"eigenvector", eigen._eigenvectors);
        opencv_core.write(fs, "mean", eigen._mean);
        Log.e("Files", "My child projection " + String.valueOf( eigen._projection.size()));
        for(int i = 0; i<eigen._projection.size(); i++) {
            opencv_core.write(fs, "projection"+String.valueOf(i), eigen._projection.get(i));
        }
        fs.release();
    }
    public static Eigenface readEigenface(String file_name, int num_components, double threshold)
    {
        FileStorage fs = new FileStorage(file_name,FileStorage.READ);
        Mat eigenvector = new Mat();
        Mat mean = new Mat();
        Vector<Mat> projection = new Vector<Mat>();
        opencv_core.read(fs.get("eigenvector"),eigenvector);
        opencv_core.read(fs.get("mean"), mean);
        for(int i = 0; i<10; i++) {
            Mat mychild = new Mat();
            opencv_core.read(fs.get("projection" + String.valueOf(i)), mychild);
            projection.add(mychild);
        }
        fs.release();
        return  new Eigenface(num_components, threshold, eigenvector,mean, projection);
    }
    public static void writeTest(String file_name, Mat eigenvector, Mat mean){
        FileStorage fs = new FileStorage(file_name,FileStorage.WRITE);
        opencv_core.write(fs,"eigenvector", eigenvector);
        opencv_core.write(fs,"mean", mean);
        fs.release();

    }

}
